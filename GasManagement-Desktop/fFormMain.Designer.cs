﻿
namespace GasManagement_Desktop
{
    partial class fFormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbResultStatus = new System.Windows.Forms.Label();
            this.btnFindCustomer = new System.Windows.Forms.Button();
            this.txbAddressFilterField = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbResultStatus);
            this.panel1.Controls.Add(this.btnFindCustomer);
            this.panel1.Controls.Add(this.txbAddressFilterField);
            this.panel1.Location = new System.Drawing.Point(31, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(468, 97);
            this.panel1.TabIndex = 0;
            // 
            // lbResultStatus
            // 
            this.lbResultStatus.AutoSize = true;
            this.lbResultStatus.Location = new System.Drawing.Point(30, 60);
            this.lbResultStatus.Name = "lbResultStatus";
            this.lbResultStatus.Size = new System.Drawing.Size(175, 15);
            this.lbResultStatus.TabIndex = 7;
            this.lbResultStatus.Text = "Không có thông tin khách hàng";
            this.lbResultStatus.Visible = false;
            // 
            // btnFindCustomer
            // 
            this.btnFindCustomer.Location = new System.Drawing.Point(372, 34);
            this.btnFindCustomer.Name = "btnFindCustomer";
            this.btnFindCustomer.Size = new System.Drawing.Size(75, 23);
            this.btnFindCustomer.TabIndex = 1;
            this.btnFindCustomer.Text = "Tìm";
            this.btnFindCustomer.UseVisualStyleBackColor = true;
            this.btnFindCustomer.Click += new System.EventHandler(this.btnFindCustomer_Click);
            // 
            // txbAddressFilterField
            // 
            this.txbAddressFilterField.Location = new System.Drawing.Point(30, 34);
            this.txbAddressFilterField.Name = "txbAddressFilterField";
            this.txbAddressFilterField.Size = new System.Drawing.Size(326, 23);
            this.txbAddressFilterField.TabIndex = 0;
            // 
            // fFormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 145);
            this.Controls.Add(this.panel1);
            this.Name = "fFormMain";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnFindCustomer;
        private System.Windows.Forms.TextBox txbAddressFilterField;
        private System.Windows.Forms.Label lbResultStatus;
    }
}

