﻿using GasManagement.Core.Infra.UoW;
using GasManagement.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Windows.Forms;

namespace GasManagement_Desktop
{
    public partial class fFormSearchCustomerResult : Form
    {
        public fFormSearchCustomerResult(string address)
        {
            InitializeComponent();
            loadCustomerData(address);
        }

        private async void loadCustomerData(string address)
        {
            using (var unitOfWork = new UnitOfWork(new GasManagementContext()))
            {
                var _retailCustomerRepository = unitOfWork.GetRepository<RetailCustomer>();
                var customer = await _retailCustomerRepository
                                .GetAll()
                                .Include(x => x.ListGasRetailCustomer)
                                .ThenInclude(x => x.Gas)
                                .Include(x => x.ListRetailCustomerPhone)
                                .FirstOrDefaultAsync(x => x.Address == address);
                tbTelephone.Text = customer?.ListRetailCustomerPhone != null ? String.Join(",", customer.ListRetailCustomerPhone.Select(x => x.PhoneNumber).ToList()) : "";
                tbAddress.Text = customer?.Address;
                tbGas.Text = customer?.ListGasRetailCustomer != null ? string.Join(",", customer.ListGasRetailCustomer.Select(x => x.Gas?.Id)) : "";
                lvHistoryOrder.Items.Clear();

                if (customer != null)
                {
                    var _orderRepository = unitOfWork.GetRepository<RetailOrder>();
                    var orderList = await _orderRepository
                                      .GetAll()
                                      .Include(x => x.Gas)
                                      .Include(x => x.Product)
                                      .Include(x => x.ProductCombo)
                                      .Include(x => x.Employee)
                                      .Where(x => x.RetailCustomerId == customer.Id)
                                      .OrderByDescending(x => x.Date)
                                      .Take(6)
                                      .ToListAsync();
                    foreach (var order in orderList)
                    {
                        var productName = order.GasId != null ? order.Gas?.AliasName : (order.ProductId != null ? order.Product?.AliasName : order.ProductCombo?.AliasName);
                        ListViewItem lvItem = new ListViewItem(order.Date.ToString("dd/MM/yyyy"));
                        lvItem.SubItems.Add(order?.Employee?.Name);
                        lvItem.SubItems.Add(productName);
                        lvItem.SubItems.Add(order?.Note ?? "");
                        lvHistoryOrder.Items.Add(lvItem);
                    }
                }
            }
        }
    }
}
