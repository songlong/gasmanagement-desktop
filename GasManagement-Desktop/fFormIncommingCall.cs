﻿using GasManagement.Core.Common.Dto.LandlineDto;
using GasManagement.Core.Common.Helper;
using GasManagement.Core.Infra.UoW;
using GasManagement.Core.Models;
using GasManagement.Core.Models.Customer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GasManagement_Desktop
{
    public partial class fFormIncommingCall : Form
    {
        string telephoneNumber;
        public fFormIncommingCall(string telephone)
        {
            InitializeComponent();
            telephoneNumber = telephone;
        }

        private async void fFormIncommingCall_LoadAsync(object sender, EventArgs e)
        {
            var t = await GetRetailCustomerInforByTelephone(telephoneNumber);
            fFormIncommingCall_Load(t);
        }

        private void fFormIncommingCall_Load(PopupInComingDto data)
        {
            var index = 0;
            var firstCustomerId = 0;
            foreach (var customer in data.ListCustomerLandline)
            {
                if (index==0)
                {
                    firstCustomerId = customer.CustomerId;
                    index++;
                }
                Button btn = new Button()
                {
                    Width = 150,
                    Height = 25
                };
                btn.Text = customer.Address;
                flpAddressContainer.Controls.Add(btn);
            }
            tbTelephone.Text = data.Telephone;
            tbGas.Text = data.ListCustomerLandline.FirstOrDefault(x => x.CustomerId == firstCustomerId).ListGasCustomerId;
            lvHistoryOrder.Items.Clear();
            if (data.ListOrderRetailCustomerLandline.FirstOrDefault() != null)
            {
                foreach (var order in data.ListOrderRetailCustomerLandline.Where(x=>x.RetailCustomerId == firstCustomerId).ToList())
                {
                    ListViewItem lvItem = new ListViewItem(order.OrderDate);
                    lvItem.SubItems.Add(order.EmployeeName);
                    lvItem.SubItems.Add(order.ProductName);
                    lvItem.SubItems.Add(order.OrderNote);
                    lvHistoryOrder.Items.Add(lvItem);
                }
            }
        }

        private async Task<PopupInComingDto> GetRetailCustomerInforByTelephone(string telephone)
        {
            using (var unitOfWork = new UnitOfWork(new GasManagementContext()))
            {
                var _retailCustomerPhoneRepository = unitOfWork.GetRepository<RetailCustomerPhone>();
                var _orderRepository = unitOfWork.GetRepository<RetailOrder>();
                var _retailCustomerRepository = unitOfWork.GetRepository<RetailCustomer>();
                var customerListId = await _retailCustomerPhoneRepository
                                       .GetAll()
                                       .Where(x => x.PhoneNumber == telephone)
                                       .Select(x => x.RetailCustomerId)
                                       .ToListAsync();
                var customerList = await _retailCustomerRepository
                       .GetAll()
                       .Include(x => x.ListGasRetailCustomer)
                       .ThenInclude(x => x.Gas)
                      .Where(x => customerListId.Contains(x.Id))
                       .ToListAsync();

                var totalOrderList = new List<RetailOrder>();
                foreach (var item in customerList)
                {
                    var orderList = await _orderRepository
                                          .GetAll()
                                          .Include(x => x.Gas)
                                          .Include(x => x.Product)
                                          .Include(x => x.ProductCombo)
                                          .Include(x => x.Employee)
                                          .Where(x => x.RetailCustomerId == item.Id)
                                          .OrderByDescending(x => x.Date)
                                          .Take(6)
                                          .ToListAsync();
                    totalOrderList = totalOrderList.Concat(orderList).ToList();
                }

                var PopupInComingDtoData = new PopupInComingDto
                {
                    ListOrderRetailCustomerLandline = totalOrderList.Select(x => x.ToOrderRetailCustomerLandlineDto()).ToList(),
                    ListCustomerLandline = customerList.Select(x => x.ToRetailCustomerLandlineDto()).ToList(),
                    Telephone = telephone
                };
                return PopupInComingDtoData;
            }
        }
    }
}
