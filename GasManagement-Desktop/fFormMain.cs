﻿using GasManagement.Core.Common.Dto.LandlineDto;
using GasManagement.Core.Common.Helper;
using GasManagement.Core.Infra.UoW;
using GasManagement.Core.Models;
using GasManagement.Core.Models.Customer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GasManagement_Desktop
{
    public partial class fFormMain : Form
    {
        public fFormMain()
        {
            InitializeComponent();
            UDP_Thread();
        }

        private async void btnFindCustomer_Click(object sender, EventArgs e)
        {
            using (var unitOfWork = new UnitOfWork(new GasManagementContext()))
            {
                var _retailCustomerRepository = unitOfWork.GetRepository<RetailCustomer>();
                var customer = await _retailCustomerRepository
                                .GetAll()
                                .Include(x => x.ListGasRetailCustomer)
                                .ThenInclude(x => x.Gas)
                                .Include(x => x.ListRetailCustomerPhone)
                                .FirstOrDefaultAsync(x => x.Address == txbAddressFilterField.Text);
                lbResultStatus.Visible = customer != null ? false : true;
                if (customer!=null)
                {
                    fFormSearchCustomerResult fForm = new fFormSearchCustomerResult(txbAddressFilterField.Text);
                    fForm.Show();
                }
            }
        }

        private void UDP_Thread()
        {
            string[] BufDatastr = null;
            string RecvBuf = null;
            Thread UDPthread = new Thread(async () =>
            {
                UdpClient MyUDP = new UdpClient(1691);
                IPEndPoint remoteHost = new IPEndPoint(IPAddress.Any, 0);
                while ((MyUDP != null))
                {
                    byte[] buf = MyUDP.Receive(ref remoteHost);
                    RecvBuf = Encoding.ASCII.GetString(buf, 0, buf.Length);
                    BufDatastr = RecvBuf.Split(",");
                    if (BufDatastr[1] != "" & !BufDatastr[1].Contains('.'))
                    {
                        this.Invoke(new Action(() => {
                            ShowfFormInCommingCall(BufDatastr[1]);
                        }));
                        
                    };
                }
            });
            UDPthread.Start();
            UDPthread.IsBackground = true;
        }
        
        private void ShowfFormInCommingCall(string number)
        {
            fFormIncommingCall fForm = new fFormIncommingCall(number);
            fForm.Show();
        }
    }
}
