﻿using System;

namespace GasManagement.Core.Infra.EntityInterface
{
    public interface IEntity
    {
        int Id { get; set; }   
    }

    public interface IDeletableEntity
    {
        bool IsDeleted { get; set; }
    }

    public interface ICreateEntity
    {
        DateTime? CreatedDate { get; set; }
    }

    public interface IUpdateEntity
    {
        DateTime? UpdatedDate { get; set; }
    }
}
