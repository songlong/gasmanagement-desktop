﻿using GasManagement.Core.Infra.EntityInterface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GasManagement.Core.Infra.UoW
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        public Repository(DbContext dbContext)
        {
            Context = dbContext;
        }

        public DbContext Context { get; set; }

        public DbSet<TEntity> TableEntity => Context.Set<TEntity>();

        /// <summary>
        /// Get all entity of table as IQueryable
        /// </summary>
        /// <returns></returns>
        public IQueryable<TEntity> GetAll()
        {
            return TableEntity;
        }

        /// <summary>
        /// Get entity by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TEntity Get(long id)
        {
            var entity = TableEntity.FirstOrDefault(x => x.Id == id);
            return entity;
        }

        /// <summary>
        /// Get entity by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<TEntity> GetAsync(long id)
        {
            return Task.FromResult(Get(id));
        }

        /// <summary>
        /// Check if it exists any entity that match predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public bool Exist(Expression<Func<TEntity, bool>> predicate)
        {
            return TableEntity.Any(predicate);
        }

        /// <summary>
        /// Check if it exists any entity that match predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public Task<bool> ExistAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.FromResult(Exist(predicate));
        }

        /// <summary>
        /// Insert entity to table
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isAutomaticallyIdentity"></param>
        /// <returns></returns>
        public TEntity Insert(TEntity entity, bool isAutomaticallyIdentity = true)
        {
            if (!isAutomaticallyIdentity)
            {
                SetIdForEntity(entity);
            }
            TableEntity.Attach(entity);
            return entity;
        }

        /// <summary>
        /// Insert entity to table
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isAutomaticallyIdentity"></param>
        /// <returns></returns>
        public Task<TEntity> InsertAsync(TEntity entity, bool isAutomaticallyIdentity = true)
        {
            return Task.FromResult(Insert(entity, isAutomaticallyIdentity));
        }

        /// <summary>
        /// Update entity properties
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TEntity Update(TEntity entity)
        {
            AttachEntity(entity);
            Context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        /// <summary>
        /// Update entity properties
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Task<TEntity> UpdateAsync(TEntity entity)
        {
            return Task.FromResult(Update(entity));
        }

        /// <summary>
        /// Delete entity by using itself
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(TEntity entity)
        {
            AttachEntity(entity);
            TableEntity.Remove(entity);
        }

        /// <summary>
        /// Delete entity by using itself
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Task DeleteAsync(TEntity entity)
        {
            return Task.Run(() => { Delete(entity); });
        }

        /// <summary>
        /// Delete entity by entity id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var entry = GetAttachedEntity(id);
            if (entry != null)
            {
                Delete(entry);
            }

            var entity = TableEntity.FirstOrDefault(x => x.Id == id);
            if (entity != null)
            {
                TableEntity.Remove(entity);
            }
        }

        /// <summary>
        /// Delete entity by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteAsync(long id)
        {
            return Task.Run(() => { Delete(id); });
        }

        /// <summary>
        /// Delete entities by predicate
        /// </summary>
        /// <param name="predicate"></param>
        public void Delete(Expression<Func<TEntity, bool>> predicate)
        {
            var selectedEntities = TableEntity.Where(predicate).ToList();
            foreach (var entity in selectedEntities)
                Delete(entity);
        }

        /// <summary>
        /// Delete entities by predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public Task DeleteAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.Run(() => { Delete(predicate); });
        }

        /// <summary>
        /// Get first or default entity by predicate (can be null)
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }

        /// <summary>
        /// Get first or default entity by predicate (can be null)
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return GetAll().FirstOrDefaultAsync(predicate);
        }

        /// <summary>
        /// Set Id of the adding entity, value is calculated by max Id of the table + 1;
        /// </summary>
        /// <param name="entity"></param>
        private void SetIdForEntity(TEntity entity)
        {
            entity.Id = MaxEntityId() + 1;
        }

        /// <summary>
        /// Get the max Id of the table.
        /// </summary>
        /// <returns>Maximum identity of the table.</returns>
        private int MaxEntityId()
        {
            return TableEntity.Max(x => x.Id);
        }

        /// <summary>
        /// Attach the entity to ChangeTracker, do nothing else.
        /// </summary>
        /// <param name="entity"></param>
        private void AttachEntity(TEntity entity)
        {
            var entry = Context.ChangeTracker.Entries().FirstOrDefault(x =>
                    x.Entity is TEntity && entity.Id == ((TEntity)x.Entity).Id);
            if (entry == null)
            {
                TableEntity.Attach(entity);
            }
        }

        /// <summary>
        /// Get attached entity (can be null)
        /// </summary>
        /// <param name="id">Identity of the entity.</param>
        /// <returns></returns>
        private TEntity GetAttachedEntity(long id)
        {
            var entry = Context.ChangeTracker.Entries()
                .FirstOrDefault(x => x.Entity is TEntity && id == ((TEntity)x.Entity).Id);
            return (TEntity) entry?.Entity;
        }
    }
}