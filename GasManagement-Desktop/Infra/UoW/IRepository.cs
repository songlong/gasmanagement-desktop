﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GasManagement.Core.Infra.UoW
{
    public interface IRepository<TEntity> where TEntity : class, IEntity
    {
        IQueryable<TEntity> GetAll();
        TEntity Get(long id);
        Task<TEntity> GetAsync(long id);
        TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression);
        bool Exist(Expression<Func<TEntity, bool>> expression);
        Task<bool> ExistAsync(Expression<Func<TEntity, bool>> expression);
        TEntity Insert(TEntity entity, bool isAutomaticallyIdentity = true);
        Task<TEntity> InsertAsync(TEntity entity, bool isAutomaticallyIdentity = true);
        TEntity Update(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        void Delete(TEntity entity);
        Task DeleteAsync(TEntity entity);
        void Delete(long id);
        Task DeleteAsync(long id);
        void Delete(Expression<Func<TEntity, bool>> expression);
        Task DeleteAsync(Expression<Func<TEntity, bool>> expression);

    }
}
