﻿using GasManagement.Core.Infra.EntityInterface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace GasManagement.Core.Infra.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        DbContext DbContext { get; }

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity;

        Task<int> SaveChangesAsync();
    }
}