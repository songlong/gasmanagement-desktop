﻿using GasManagement.Core.Infra.EntityInterface;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasManagement.Core.Infra.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbContext _dbContext { get; }
        private Dictionary<Type, object> Repositories { get; set; }
        private bool disposed;

        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentException();
        }

        public DbContext DbContext => _dbContext;

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity
        {
            if (Repositories == null)
            {
                Repositories = new Dictionary<Type, object>();
            }
            var type = typeof(TEntity);
            if (!Repositories.ContainsKey(type))
            {
                Repositories[type] = new Repository<TEntity>(DbContext);
            }
            return (IRepository<TEntity>)Repositories[type];
        }

        public async Task<int> SaveChangesAsync()
        {
            var changesSet = DbContext?.ChangeTracker?.Entries() ?? null;
            AddAuditForEntries(changesSet.ToList());
            var result = await DbContext.SaveChangesAsync();
            return result;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    DbContext.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void AddAuditForEntries(List<EntityEntry> entries)
        {
            var changedEntries = entries
                .Where(x => x.Entity is IEntity && x.State != EntityState.Unchanged && x.State != EntityState.Detached);

            foreach (var entry in entries)
            {
                if (!(entry.Entity is IEntity))
                {
                    continue;
                }
                //var currentUserId = _identityInformation.GetUserId() ?? 0;
                if (entry.State == EntityState.Added)
                {
                    if (entry.Entity is IDeletableEntity deletableEntity)
                    {
                        deletableEntity.IsDeleted = false;
                    }
                    if (entry.Entity is ICreateEntity modifiedEntity)
                    {
                        //modifiedEntity.CreatedBy = currentUserId;
                        modifiedEntity.CreatedDate = DateTime.Now;
                    }
                }
                else if (entry.State == EntityState.Deleted)
                {
                    if (entry.Entity is IDeletableEntity deletableEntity)
                    {
                        deletableEntity.IsDeleted = true;
                    }
                    entry.State = EntityState.Modified;
                }
                if (entry.State == EntityState.Modified)
                {
                    if (entry.Entity is IUpdateEntity modifiedEntity)
                    {
                        //modifiedEntity.UpdatedBy = currentUserId;
                        modifiedEntity.UpdatedDate = DateTime.Now;
                    }
                }

            }
        }
    }
}
