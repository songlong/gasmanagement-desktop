﻿using GasManagement.Core.Infra.UoW;
using GasManagement.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Configuration;

namespace GasManagement.Core.Infra.AppConfiguration
{
    public static class AppCoreRegister
    {
        public static void RegisterCoreServices(this IServiceCollection services)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["BloggingDatabase"].ConnectionString;
            // Database
            services.AddDbContext<GasManagementContext>(options => options.UseSqlServer(connectionString));
            services.AddScoped<DbContext, GasManagementContext>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}
