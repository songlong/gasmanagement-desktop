﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class RetailDebtOrder : IEntity, IDeletableEntity
    {
        [Column("RetailDebtOrderId")]
        public int Id { get; set; }
        public int RetailOrderId { get; set; }
        public int TypeDebtId { get; set; }
        public DateTime? Payday { get; set; }
        public bool IsDeleted { get; set; }
        public TypeDebt TypeDebt { get; set; }
        public RetailOrder RetailOrder { get; set; }
    }
}
