﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class WholesaleDebtOrder : IEntity, IDeletableEntity
    {
        [Column("WholesaleDebtOrderId")]
        public int Id { get; set; }
        public int WholesaleOrderId { get; set; }
        public int TypeDebtId { get; set; }
        public DateTime? Payday { get; set; }
        public bool IsDeleted { get; set; }
        public TypeDebt TypeDebt { get; set; }
        public WholesaleOrder WholesaleOrder { get; set; }
    }
}
