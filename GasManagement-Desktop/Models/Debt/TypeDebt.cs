﻿using GasManagement.Core.Infra.EntityInterface;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class TypeDebt : IEntity, IDeletableEntity
    {
        [Column("TypeDebtId")]
        public int Id { get; set; }
        public string Description {get;set;}
        public bool IsDeleted { get; set; }
    }
}
