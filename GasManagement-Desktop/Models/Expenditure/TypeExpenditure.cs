﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GasManagement.Core.Models
{
    public class TypeExpenditure : IEntity, IDeletableEntity
    {
        [Column("TypeExpenditureId")]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
