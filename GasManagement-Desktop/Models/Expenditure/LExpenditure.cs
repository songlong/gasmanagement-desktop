﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GasManagement.Core.Models
{
    public class LExpenditure : IEntity, IDeletableEntity
    {
        [Column("LExpenditureId")]
        public int Id { get; set; }
        public int TypeExpenditureId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public virtual TypeExpenditure TypeExpenditure { get; set; }
    }
}
