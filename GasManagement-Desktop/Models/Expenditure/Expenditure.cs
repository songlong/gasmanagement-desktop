﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GasManagement.Core.Models
{
    public class Expenditure : IEntity, IDeletableEntity
    {
        [Column("ExpenditureId")]
        public int Id { get; set; }
        public DateTime PaydayMoney { get; set; }
        public int LExpenditureId { get; set; }
        public decimal Money { get; set; }
        public string Note { get; set; }
        public bool IsDeleted { get; set; }
        public virtual LExpenditure LExpenditure { get; set; }
    }
}
