﻿using GasManagement.Core.Models.Customer;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace GasManagement.Core.Models
{
    public class GasManagementContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["BloggingDatabase"].ConnectionString);
        }
        public DbSet<Product> Product { get; set; }
        public DbSet<Gas> Gas { get; set; }
        public DbSet<RetailOrder> RetailOrder { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<GasRetailCustomer> GasRetailCustomer { get; set; }
        public DbSet<RetailCustomer> RetailCustomer { get; set; }
        public DbSet<RetailCustomerPhone> RetailCustomerPhone { get; set; }
    }
}