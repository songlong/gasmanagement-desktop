﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GasManagement.Core.Models.Home
{
    public class SalesGraphAllTypesGas
    {
        public DateTime Date { get; set; }
        public int TotalAmountGas { get; set; }
    }
}
