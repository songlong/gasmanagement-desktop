﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class GiftPrice : IEntity, IDeletableEntity
    {
        [Column("GiftPriceId")]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int GiftId { get; set; }
        [Column(TypeName = "money")]
        public decimal ImportPrice { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("GiftId")]
        public Gift Gift { get; set; }
    }
}
