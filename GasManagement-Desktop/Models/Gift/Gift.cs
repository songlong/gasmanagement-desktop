﻿using GasManagement.Core.Infra.EntityInterface;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class Gift : IEntity, IDeletableEntity
    {
        [Column("GiftId")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public virtual List<GiftPrice> GiftPrice { get; set; }
        public virtual List<GiftStorage> GiftStorage { get; set; }
        public virtual List<HistoryImportGift> HistoryImportGift { get; set; }
    }
}
