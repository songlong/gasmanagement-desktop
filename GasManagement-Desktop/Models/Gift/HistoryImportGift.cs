﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class HistoryImportGift : IEntity, IDeletableEntity
    {
        [Column("HistoryImportGiftId")]
        public int Id { get; set; }
        public DateTime DateImport { get; set; }
        public int GiftId { get; set; }
        public int Amount { get; set; }
        public DateTime? PaydayMoney { get; set; }
        public string Note { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("GiftId")]
        public Gift Gift { get; set; }
    }
}
