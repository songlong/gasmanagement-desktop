﻿using GasManagement.Core.Infra.EntityInterface;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class GiftStorage : IEntity, IDeletableEntity

    {
        [Column("GiftStorageId")]
        public int Id { get; set; }
        public int GiftId { get; set; }
        public int Amount { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("GiftId")]
        public Gift Gift { get; set; }
    }
}
