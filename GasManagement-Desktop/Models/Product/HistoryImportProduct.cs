﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class HistoryImportProduct : IEntity, IDeletableEntity
    {
        [Column("HistoryImportProductId")]
        public int Id { get; set; }
        public DateTime DateImport { get; set; }
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public DateTime? PaydayMoney { get; set; }
        public string Note { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
    }
}
