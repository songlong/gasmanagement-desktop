﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class ProductComboPrice : IEntity, IDeletableEntity
    {
        [Column("ProductComboPriceId")]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int ProductComboId { get; set; }
        public decimal WholesalePrice { get; set; }
        public decimal RetailPrice { get; set; }
        public bool IsDeleted { get; set; }
    }
}
