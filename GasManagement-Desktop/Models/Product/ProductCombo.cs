﻿using GasManagement.Core.Infra.EntityInterface;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class ProductCombo : IEntity, IDeletableEntity
    {
        [Column("ProductComboId")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public virtual List<ProductComboPrice> ProductComboPrice { get; set; }
        public virtual List<ProductComboDetail> ProductComboDetail { get; set; }
    }
}
