﻿using GasManagement.Core.Infra.EntityInterface;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class ProductStorage : IEntity, IDeletableEntity

    {
        [Column("ProductStorageId")]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
    }
}
