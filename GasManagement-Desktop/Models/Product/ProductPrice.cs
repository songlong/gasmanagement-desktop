﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    [Table("ProductPrice")]
    public class ProductPrice : IEntity, IDeletableEntity
    {
        [Column("ProductPriceId")]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int ProductId { get; set; }
        [Column(TypeName = "money")]
        public decimal ImportPrice { get; set; }
        [Column(TypeName = "money")]
        public decimal WholesalePrice { get; set; }
        [Column(TypeName = "money")]
        public decimal RetailPrice { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
    }
}
