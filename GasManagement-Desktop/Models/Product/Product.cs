﻿using GasManagement.Core.Infra.EntityInterface;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class Product : IEntity, IDeletableEntity
    {
        [Column("ProductId")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public virtual List<ProductPrice> ProductPrice { get; set; }
        public virtual ProductStorage ProductStorage { get; set; }
        public virtual List<HistoryImportProduct> HistoryImportProduct { get; set; }
    }
}
