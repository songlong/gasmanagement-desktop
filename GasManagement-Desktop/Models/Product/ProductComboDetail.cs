﻿using GasManagement.Core.Infra.EntityInterface;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class ProductComboDetail : IEntity, IDeletableEntity
    {
        [Column("ProductComboDetailId")]
        public int Id { get; set; }
        public int ProductComboId { get; set; }
        public int? ProductId { get; set; }
        public int? GasId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
