﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GasManagement.Core.Models.Marketing
{
    public class CustomerAndTwoDate
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Gas { get; set; }
        public DateTime RecentDate { get; set; }
        public DateTime RecentSecondDate { get; set; }
    }
}
