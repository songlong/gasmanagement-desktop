﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GasManagement.Core.Models.Marketing
{
    public class CustomerDate
    {
        public int Id {get;set;}
        public string Address { get; set; }
        public string Gas { get; set; }
        public DateTime Date { get; set; }
    }
}
