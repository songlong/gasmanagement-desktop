﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class NewOrMissRetailCustomer : IEntity, IDeletableEntity
    {
        [Column("NewOrMissRetailCustomerId")]
        public int Id { get; set; }
        public int RetailCustomerId { get; set; }
        public int CustomerStatusId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateTakeGas { get; set; }
        public int? EmployeeId { get; set; }
        public bool IsDeleted { get; set; }
        public RetailCustomer RetailCustomer { get; set; }
        public Employee Employee { get; set; }
        public CustomerStatus CustomerStatus { get; set; }
    }
}
