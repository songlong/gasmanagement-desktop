﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class CustomerStatus : IEntity, IDeletableEntity
    {
        [Column("CustomerStatusId")]
        public int Id { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
    }
}
