﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class WholesaleOrder : IEntity, IDeletableEntity
    {
        [Column("WholesaleOrderId")]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int WholesaleCustomerId { get; set; }
        public int? GasId { get; set; }
        public int? ProductId { get; set; }
        public int? GiftId { get; set; }
        public string Note { get; set; }
        public decimal? RealMoneyTaken { get; set; }
        public bool IsDeleted { get; set; }
        public WholesaleCustomer WholesaleCustomer { get; set; }
        public Gas Gas { get; set; }
        public Gift Gift { get; set; }
        public Product Product { get; set; }
        public List<WholesaleDebtOrder> WholesaleDebtOrder { get; set; }
    }
}
