﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class RetailOrder : IEntity, IDeletableEntity
    {
        [Column("RetailOrderId")]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int RetailCustomerId { get; set; }
        public string? PhoneNumber { get; set; }
        public int? GasId { get; set; }
        public int? ProductId { get; set; }
        public int? ProductComboId { get; set; }
        public int? GiftId { get; set; }
        public int? EmployeeId { get; set; }
        public string? Note { get; set; }
        public int? ValuePoint { get; set; }
        public decimal? RealMoneyTaken { get; set; }
        public bool IsDeleted { get; set; }
        public RetailCustomer? Customer{ get; set; }
        public Gas? Gas { get; set; }
        public Gift? Gift { get; set; }
        public Product? Product { get; set; }
        public ProductCombo? ProductCombo { get; set; }
        public Employee? Employee { get; set; }
        public List<RetailDebtOrder>? RetailDebtOrder { get; set; }
    }
}
