﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class HistoryImportGas : IEntity, IDeletableEntity
    {
        [Column("HistoryImportGasId")]
        public int Id { get; set; }
        public DateTime DateImport { get; set; }
        public int GasId { get; set; }
        public int GasWaterAmountImport { get; set; }
        public int GasShellAmountExport { get; set; }
        public DateTime? PaydayMoney { get; set; }
        public string Note { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("GasId")]
        public Gas Gas { get; set; }
    }
}
