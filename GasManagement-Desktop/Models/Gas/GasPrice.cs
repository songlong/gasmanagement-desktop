﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    [Table("GasPrices")]
    public class GasPrice : IEntity, IDeletableEntity
    {
        [Column("GasPriceId")]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int GasId { get; set; }
        [Column(TypeName = "money")]
        public decimal ImportPrice { get; set; }
        [Column(TypeName = "money")]
        public decimal WholesalePrice { get; set; }
        [Column(TypeName = "money")]
        public decimal RetailPrice { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("GasId")]
        public Gas Gas { get; set; }
    }
}
