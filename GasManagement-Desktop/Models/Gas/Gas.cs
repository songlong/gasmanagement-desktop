﻿using GasManagement.Core.Infra.EntityInterface;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class Gas : IEntity, IDeletableEntity
    {
        [Column("GasId")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public virtual List<GasPrice> GasPrices { get; set; }
        public virtual GasStorage GasStorage { get; set; }
        public virtual List<HistoryImportGas> HistoryImportGas { get; set; }
    }
}
