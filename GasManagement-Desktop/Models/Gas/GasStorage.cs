﻿using GasManagement.Core.Infra.EntityInterface;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class GasStorage : IEntity, IDeletableEntity

    {
        [Column("GasStorageId")]
        public int Id { get; set; }
        public int GasId { get; set; }
        public int GasWaterAmount { get; set; }
        public int GasShellAmount { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("GasId")]
        public Gas Gas { get; set; }
    }
}
