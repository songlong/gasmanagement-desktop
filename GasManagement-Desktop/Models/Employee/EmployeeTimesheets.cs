﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class EmployeeTimesheets : IEntity, IDeletableEntity
    {
        [Column("EmployeeTimesheetsId")]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public int ValuePoint { get; set; }
        public bool IsDeleted { get; set; }
    }
}
