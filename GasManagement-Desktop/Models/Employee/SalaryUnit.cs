﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class SalaryUnit : IEntity, IDeletableEntity
    {
        [Column("SalaryUnitId")]
        public int Id { get; set; }
        public DateTime DateStart { get; set; }
        public decimal SalaryUnitValue { get; set; }
        public bool IsDeleted { get; set; }
    }
}
