﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class CashAdvance : IEntity, IDeletableEntity
    {
        [Column("CashAdvanceId")]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime DateReceivingMoney { get; set; }
        public decimal NumberMoney { get; set; }
        public bool IsDeleted { get; set; }
    }
}
