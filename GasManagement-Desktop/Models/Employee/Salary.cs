﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class Salary : IEntity, IDeletableEntity
    {
        [Column("SalaryId")]
        public int Id { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmployeeId { get; set; }
        public int ValuePoint { get; set; }
        public decimal ValuePointMoney { get; set; }
        public decimal AdvanceMoney { get; set; }
        public decimal BonusMoney { get; set; }
        public DateTime Payday { get; set; }
        public bool IsDeleted { get; set; }
    }
}
