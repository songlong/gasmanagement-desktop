﻿using GasManagement.Core.Infra.EntityInterface;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class Employee : IEntity, IDeletableEntity
    {
        [Column("EmployeeId")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public DateTime? Birthday { get; set; }
        public string NumberIdentity { get; set; }
        public string Image { get; set; }
        public string HomeTown { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
    }
}
