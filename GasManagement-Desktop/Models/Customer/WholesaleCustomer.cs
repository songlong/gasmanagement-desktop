﻿using GasManagement.Core.Infra.EntityInterface;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class WholesaleCustomer : IEntity, IDeletableEntity
    {
        [Column("WholesaleCustomerId")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public bool IsDeleted { get; set; }
    }
}
