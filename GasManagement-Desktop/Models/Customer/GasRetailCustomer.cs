﻿using GasManagement.Core.Infra.EntityInterface;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class GasRetailCustomer : IEntity, IDeletableEntity
    {
        [Column("GasRetailCustomerId")]
        public int Id { get; set; }
        public int RetailCustomerId { get; set; }
        public int GasId { get; set; }
        public bool IsDeleted { get; set; }
        public RetailCustomer RetailCustomer { get; set; }
        public Gas Gas { get; set; }
    }
}
