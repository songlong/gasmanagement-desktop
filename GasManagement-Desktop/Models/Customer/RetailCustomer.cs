﻿using GasManagement.Core.Infra.EntityInterface;
using GasManagement.Core.Models.Customer;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models
{
    public class RetailCustomer : IEntity, IDeletableEntity
    {
        [Column("RetailCustomerId")]
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Address { get; set; }
        public string? Note { get; set; }
        public bool IsDeleted { get; set; }
        public virtual List<GasRetailCustomer>? ListGasRetailCustomer { get; set; }
        public virtual List<RetailCustomerPhone>? ListRetailCustomerPhone { get; set; }
    }
}
