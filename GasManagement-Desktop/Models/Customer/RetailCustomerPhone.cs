﻿using GasManagement.Core.Infra.EntityInterface;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasManagement.Core.Models.Customer
{
    public class RetailCustomerPhone: IEntity, IDeletableEntity
    {
        [Column("RetailCustomerPhoneId")]
        public int Id { get; set; }
        public int RetailCustomerId { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Note { get; set; }
        public bool IsDeleted { get; set; }
    }
}
