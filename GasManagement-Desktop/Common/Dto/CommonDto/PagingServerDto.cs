﻿namespace GasManagement.Core.Common.Dto.CommonDto
{
    public class PagingServerDto<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int RecordsTotal { get; set; }
        public T[] Data { get; set; }
        public int RecordsFiltered => Data?.Length ?? 0;
        public int Draw { get; set; }
    }
}
