﻿using System;

namespace GasManagement.Core.Common.Dto.CommonDto
{
    public class FileInfoDto
    {
        /// <summary>
        /// Full name of file
        /// </summary>
        public string FullFileName { get; set; }

        public string FileName { get; set; }

        public string Extension { get; set; }

        public Guid Guid { get; set; }

        public string FullGuidFileName { get; set; } = string.Empty;
    }
}
