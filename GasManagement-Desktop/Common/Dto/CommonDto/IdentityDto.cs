﻿namespace GasManagement.Core.Common.Dto.CommonDto
{
    public class IdentityDto
    {
        public int Id { get; set; }
    }
}
