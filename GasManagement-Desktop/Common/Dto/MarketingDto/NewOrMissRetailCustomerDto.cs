﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GasManagement.Core.Common.Dto.MarketingDto
{
    public class NewOrMissRetailCustomerDto
    {
        public int RetailCustomerId { get; set; }
        public string Address { get; set; }
        public DateTime DateCreated { get; set; }
        public string CustomerStatus { get; set; }
        public DateTime? DateTakeGas { get; set; }
        public string MarketingStaff { get; set; }
    }
}
