﻿namespace GasManagement.Core.Common.Dto.ProductDto
{
    public class ImportProductDto
    {
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public bool IsPaid { get; set; }
        public string Note { get; set; }
    }
}
