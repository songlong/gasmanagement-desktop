﻿using System;

namespace GasManagement.Core.Common.Dto.ProductDto
{
    public class HistoryImportProductDto
    {
        public int HistoryImportProductId { get; set; }
        public DateTime DateImport { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        public int Amount { get; set; }
        public DateTime? PaydayMoney { get; set; }
        public decimal ProductPriceUnit { get; set; }
        public string Note { get; set; }
        public string Function { get; set; }
    }
}
