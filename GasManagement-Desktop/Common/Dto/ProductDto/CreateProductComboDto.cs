﻿using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto.ProductDto
{
    public class CreateProductComboDto
    {
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal WholesalePrice { get; set; }
        public List<ItemProductComboDto> ListItemProductCombo { get; set; }
    }

    public class ItemProductComboDto
    {
        public int? GasId { get; set; }
        public int? ProductId { get; set; }
    }
}
