﻿namespace GasManagement.Core.Common.Dto.ProductDto
{
    public class CreateProductDto
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public decimal ImportPrice { get; set; }
        public decimal WholesalePrice { get; set; }
        public decimal RetailPrice { get; set; }
        public int Amount { get; set; }
    }
}
