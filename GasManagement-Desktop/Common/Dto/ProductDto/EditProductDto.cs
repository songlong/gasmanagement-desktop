﻿namespace GasManagement.Core.Common.Dto.ProductDto
{
    public class EditProductDto : CreateProductDto
    {
        public int ProductId { get; set; }
    }
}
