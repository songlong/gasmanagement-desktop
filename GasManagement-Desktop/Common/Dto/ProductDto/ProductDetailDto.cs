﻿using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto.ProductDto
{
    public class ProductDetailDto
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public decimal ImportPrice { get; set; }
        public decimal WholesalePrice { get; set; }
        public decimal RetailPrice { get; set; }
        public int Amount { get; set; }
        public string Function { get; set; }
        public List<HistoryImportProductDto> ListHistoryImportProduct { get; set; }
    }
}
