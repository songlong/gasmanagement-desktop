﻿namespace GasManagement.Core.Common.Dto.UserDto
{
    public class UserInfoDto
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => (FirstName ?? string.Empty) + " " + (LastName ?? string.Empty);
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
    }
}
