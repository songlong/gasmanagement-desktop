﻿using System;
using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto
{
    public class ExpenditureDetailDto
    {
        public int ExpenditureId { get; set; }
        public DateTime PaydayMoney { get; set; }
        public string TypeExpenditure { get; set; }
        public string LExpenditure { get; set; }
        public decimal Money { get; set; }
        public string Note { get; set; }
        public string Function { get; set; }
    }
}
