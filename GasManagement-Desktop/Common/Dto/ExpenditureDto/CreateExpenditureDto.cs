﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GasManagement.Core.Common.Dto.ExpenditureDto
{
    public class CreateExpenditureDto
    {
        public DateTime PaydayMoney { get; set; }
        public int LExpenditureId { get; set; }
        public decimal Money { get; set; }
        public string Note { get; set; }
    }
}
