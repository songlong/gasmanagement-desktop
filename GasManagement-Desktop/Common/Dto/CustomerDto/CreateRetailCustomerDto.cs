﻿using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto.CustomerDto
{
    public class CreateRetailCustomerDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Note { get; set; }
        public List<int> ListGasId{ get; set; }
    }
}
