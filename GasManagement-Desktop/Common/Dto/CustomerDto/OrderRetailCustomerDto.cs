﻿using System;

namespace GasManagement.Core.Common.Dto.CustomerDto
{
    public class OrderRetailCustomerDto
    {
        public string OrderDate { get; set; }
        public string ProductName { get; set; }
        public string EmployeeName { get; set; }
        public string OrderNote { get; set; }
    }
}
