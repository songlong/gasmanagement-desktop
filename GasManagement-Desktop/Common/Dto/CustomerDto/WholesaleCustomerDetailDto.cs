﻿namespace GasManagement.Core.Common.Dto.CustomerDto
{
    public class WholesaleCustomerDetailDto
    {
        public int WholesaleCustomerId { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public string Function { get; set; }
    }
}
