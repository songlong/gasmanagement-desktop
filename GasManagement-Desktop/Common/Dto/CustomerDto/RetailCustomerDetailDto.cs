﻿namespace GasManagement.Core.Common.Dto.CustomerDto
{
    public class RetailCustomerDetailDto
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public string ListGasCustomerName { get; set; }
        public string ListGasCustomerId { get; set; }
        public string Function { get; set; }
    }
}
