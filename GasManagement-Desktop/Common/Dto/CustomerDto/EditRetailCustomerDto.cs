﻿namespace GasManagement.Core.Common.Dto.CustomerDto
{
    public class EditRetailCustomerDto : CreateRetailCustomerDto
    {
        public int CustomerId { get; set; }
    }
}
