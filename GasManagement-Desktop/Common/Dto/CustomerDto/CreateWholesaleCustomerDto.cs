﻿namespace GasManagement.Core.Common.Dto.CustomerDto
{
    public class CreateWholesaleCustomerDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Note { get; set; }
    }
}
