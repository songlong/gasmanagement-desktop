﻿using System;

namespace GasManagement.Core.Common.Dto.EmployeeDto
{
    public class CreateEmployeeSalaryDto
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmployeeId { get; set; }
        public decimal BonusMoney { get; set; }
    }
}