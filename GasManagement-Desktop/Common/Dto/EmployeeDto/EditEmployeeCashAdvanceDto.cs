﻿namespace GasManagement.Core.Common.Dto.EmployeeDto
{
    public class EditEmployeeCashAdvanceDto : CreateEmployeeCashAdvanceDto
    {
        public int CashAdvanceId { get; set; }
    }
}
