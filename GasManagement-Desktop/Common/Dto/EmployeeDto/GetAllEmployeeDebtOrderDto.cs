﻿using System;

namespace GasManagement.Core.Common.Dto.EmployeeDto
{
    public class GetAllEmployeeDebtOrderDto
    {
        public int DebtOrderId { get; set; }
        public DateTime DebtDay { get; set; }
        public string Address { get; set; }
        public string Product { get; set; }
        public string Note { get; set; }
        public string Employee { get; set; }
        public string Function { get; set; }
    }
}