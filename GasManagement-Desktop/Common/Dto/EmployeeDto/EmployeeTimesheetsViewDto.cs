﻿using System;

namespace GasManagement.Core.Common.Dto.EmployeeDto
{
    public class EmployeeTimesheetsViewDto
    {
        public DateTime Date { get; set; }
        public int ValuePoint { get; set; }
    }
}
