﻿using System;

namespace GasManagement.Core.Common.Dto.EmployeeDto
{
    public class CreateEmployeeCashAdvanceDto
    {
        public int EmployeeId { get; set; }
        public DateTime DateReceivingMoney { get; set; }
        public decimal NumberMoney { get; set; }
    }
}
