﻿namespace GasManagement.Core.Common.Dto.EmployeeDto
{
    public class DataForCreatePaySalaryDto
    {
        public decimal CashAdvanceMoney { get; set; }
        public int ValuePoint { get; set; }
        public decimal ValuePointMoney { get; set; }
    }
}
