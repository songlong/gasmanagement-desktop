﻿using System;

namespace GasManagement.Core.Common.Dto.EmployeeDto
{
    public class EmployeeCashAdvanceViewDto
    {
        public int CashAdvanceId { get; set; }
        public DateTime DateReceivingMoney { get; set; }
        public decimal NumberMoney { get; set; }
        public string Function { get; set; }
    }
}
