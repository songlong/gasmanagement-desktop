﻿using System;

namespace GasManagement.Core.Common.Dto.EmployeeDto
{
    public class SalaryViewDto
    {
        public int SalaryId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int ValuePoint { get; set; }
        public decimal ValuePointMoney { get; set; }
        public decimal AdvanceMoney { get; set; }
        public decimal BonusMoney { get; set; }
        public DateTime Payday { get; set; }
        public string Function { get; set; }
    }
}
