﻿using System;

namespace GasManagement.Core.Common.Dto.EmployeeDto
{
    public class CreateEmployeeDto
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string AliasName { get; set; }
        public DateTime Birthday { get; set; }
        public string NumberIdentity { get; set; }
        public string HomeTown { get; set; }
    }
}




