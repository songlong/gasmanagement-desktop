﻿using System.ComponentModel.DataAnnotations;

namespace GasManagement.Core.Common.Dto.AuthenticateDto
{
    public class LoginDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        //public string ReturnUrl { get; set; }
    }
}
