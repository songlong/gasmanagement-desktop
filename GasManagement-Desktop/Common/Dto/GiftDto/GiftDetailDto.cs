﻿namespace GasManagement.Core.Common.Dto.GiftDto
{
    public class GiftDetailDto
    {
        public int GiftId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public decimal ImportPrice { get; set; }
        public int Amount { get; set; }
        public string Function { get; set; }
    }
}
