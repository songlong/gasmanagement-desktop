﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasManagement.Core.Common.Dto.GiftDto
{
    public class HistoryImportGiftDto
    {
        public int HistoryImportGiftId { get; set; }
        public DateTime DateImport { get; set; }
        public string GiftName { get; set; }
        public string GiftImage { get; set; }
        public int Amount { get; set; }
        public DateTime? PaydayMoney { get; set; }
        public decimal GiftPriceUnit { get; set; }
        public string Note { get; set; }
        public string Function { get; set; }
    }
}
