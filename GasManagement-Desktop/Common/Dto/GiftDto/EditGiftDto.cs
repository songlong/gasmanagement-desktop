﻿namespace GasManagement.Core.Common.Dto.GiftDto
{
    public class EditGiftDto : CreateGiftDto
    {
        public int GasId { get; set; }
    }
}
