﻿namespace GasManagement.Core.Common.Dto.GiftDto
{
    public class ImportGiftDto
    {
        public int GiftId { get; set; }
        public int GiftAmountImport { get; set; }
        public bool IsPaid { get; set; }
        public string Note { get; set; }
    }
}
