﻿namespace GasManagement.Core.Common.Dto.OrderDto
{
    public class CreateRetailOrderDto
    {
        public string CustomerName { get; set; }
        public string CustomerAddress{ get; set; }
        public string CustomerTelephone { get; set; }
        public decimal? ProductPrice { get; set; }
        public int? GasId { get; set; }
        public int? ProductId { get; set; }
        public int? ProductComboId { get; set; }
        public int? GiftId { get; set; }
        public int? EmployeeId { get; set; }
        public string Note { get; set; }
    }
}
