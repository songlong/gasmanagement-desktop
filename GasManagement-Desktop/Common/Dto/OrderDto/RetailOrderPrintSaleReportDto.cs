﻿using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto.OrderDto
{
    public class RetailOrderPrintSaleReportDto
    {
        public string Employee { get; set; }
        public List<RetailOrderSaleReport> ListOrder { get; set; }
    }

    public class RetailOrderSaleReport
    {
        public string Address { get; set; }
        public string ProductAliasName { get; set; }
        public string ProductPrice { get; set; }
        public string Note { get; set; }
        public int ValuePoint { get; set; }
    }
}
