﻿using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto.OrderDto
{
    public class EditRetailOrderDto : CreateRetailOrderDto
    {
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public List<string> Debt { get; set; }
    }
}
