﻿using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto.OrderDto
{
    public class RetailOrderDetailDto
    {
        public int OrderId { get; set; }
        public int RetailCustomerId { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public int? GasId { get; set; }
        public string GasAliasName { get; set; }
        public int? ProductId { get; set; }
        public string ProductAliasName { get; set; }
        public int? ProductComboId { get; set; }
        public string ProductComboAliasName { get; set; }
        public int? GiftId { get; set; }
        public string GiftName { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeAliasName { get; set; }
        public string Note { get; set; }
        public decimal RealMoneyCollect { get; set; }
        public int ValuePoint { get; set; }
        public List<string> Debt { get; set; }
    }
}
