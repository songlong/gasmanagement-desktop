﻿namespace GasManagement.Core.Common.Dto.OrderDto
{
    public class EditRealValueReportRetailOrderDto
    {
        public int OrderId { get; set; }
        public int ValuePoint { get; set; }
        public decimal RealMoneyCollect { get; set; }
    }
}
