﻿using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto
{
    public class GasBasicDataDto
    {
        public int GasId { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public decimal RetailPrice { get; set; }
    }
}
