﻿using System;

namespace GasManagement.Core.Common.Dto.CustomerDto
{
    public class OrderRetailCustomerLandlineDto
    {
        public int RetailCustomerId { get; set; }
        public string OrderDate { get; set; }
        public string ProductName { get; set; }
        public string EmployeeName { get; set; }
        public string OrderNote { get; set; }
    }
}
