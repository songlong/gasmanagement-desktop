﻿using GasManagement.Core.Common.Dto.CustomerDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace GasManagement.Core.Common.Dto.LandlineDto
{
    public class PopupInComingDto
    {
        public List<CustomerLandlineDto> ListCustomerLandline { get; set; }
        public List<OrderRetailCustomerLandlineDto> ListOrderRetailCustomerLandline { get; set; }
        public string Telephone { get; set; }
    }
}
