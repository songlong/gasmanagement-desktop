﻿using GasManagement.Core.Common.Dto.CustomerDto;
using GasManagement.Core.Models;
using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto
{
    public class CustomerLandlineDto
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public string ListGasCustomerId { get; set; }
    }
}
