﻿using System.Collections.Generic;

namespace GasManagement.Core.Common.Dto.GasDto
{
    public class GasDetailDto
    {
        public int GasId { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public decimal ImportPrice { get; set; }
        public decimal WholesalePrice { get; set; }
        public decimal RetailPrice { get; set; }
        public int GasWaterAmount { get; set; }
        public int GasShellAmount { get; set; }
        public string Function { get; set; }
        public List<HistoryImportGasDto> ListHistoryImportGasDto { get; set; }
    }
}
