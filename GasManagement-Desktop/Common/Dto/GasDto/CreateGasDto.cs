﻿namespace GasManagement.Core.Common.Dto.GasDto
{
    public class CreateGasDto
    {
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public decimal ImportPrice { get; set; }
        public decimal WholesalePrice { get; set; }
        public decimal RetailPrice { get; set; }
        public int GasWaterAmount { get; set; }
        public int GasShellAmount { get; set; }
    }
}
