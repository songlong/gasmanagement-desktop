﻿namespace GasManagement.Core.Common.Dto.GasDto
{
    public class ImportGasDto
    {
        public int GasId { get; set; }
        public int GasWaterImport { get; set; }
        public int GasShellExport { get; set; }
        public bool IsPaid { get; set; }
        public string Note { get; set; }
    }
}
