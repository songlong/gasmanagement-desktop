﻿namespace GasManagement.Core.Common.Dto.GasDto
{
    public class EditGasDto : CreateGasDto
    {
        public int GasId { get; set; }
    }
}
