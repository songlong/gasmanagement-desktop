﻿using System;

namespace GasManagement.Core.Common.Dto.GasDto
{
    public class HistoryImportGasDto
    {
        public int HistoryImportGasId { get; set; }
        public DateTime DateImport { get; set; }
        public int GasId { get; set; }
        public string GasName { get; set; }
        public string GasImage { get; set; }
        public int GasShellAmountExport { get; set; }
        public int GasWaterAmountImport { get; set; }
        public DateTime? PaydayMoney { get; set; }
        public decimal GasPriceUnit { get; set; }
        public string Note { get; set; }
        public string Function { get; set; }
    }
}
