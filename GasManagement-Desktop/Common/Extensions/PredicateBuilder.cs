﻿using System;
using System.Linq.Expressions;

namespace GasManagement.Core.Common.Extensions
{
    public static class PredicateBuilder
    {
        /// <summary>
        /// Creates a Expression that represents a conditional OR operation 
        /// that evaluates the second operand only if the first operand evaluates to false.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression1"></param>
        /// <param name="expression2"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> OrElse<T>(this Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
        {
            return Expression.Lambda<Func<T, bool>>(Expression.OrElse(expression1.Body, expression2.Body), expression1.Parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression1"></param>
        /// <param name="expression2"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
        {
            return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(expression1.Body, expression2.Body), expression1.Parameters);
        }
    }
}
