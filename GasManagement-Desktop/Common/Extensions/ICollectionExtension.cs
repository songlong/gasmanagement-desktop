﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace GasManagement.Core.Common.Extensions
{
    public static class ICollectionExtension
    {
        /// <summary>
        /// Determines whether a sequence is not null and contains any element
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static bool HasAny<T>(this ICollection<T> collection)
        {
            return collection != null && collection.Any();
        }

        /// <summary>
        /// Determines whether a sequence is not null and contains any element base on predicate
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool HasAny<T>(this ICollection<T> collection, Func<T, bool> predicate)
        {
            return collection != null && collection.Any(predicate);
        }

        /// <summary>
        /// Filter a sequence of values based on a predicate if condition return true
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="condition"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IEnumerable<T> WhereIf<T>(this IEnumerable<T> source, bool condition, Func<T, bool> predicate)
        {
            return !condition
                ? source
                : source.Where(predicate);
        }

        /// <summary>
        /// Filter a sequence of values based on a predicate if condition return true
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="condition"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IQueryable<T> WhereIf<T>(this IQueryable<T> source, bool condition, Expression<Func<T, bool>> predicate)
        {
            return !condition
                ? source
                : source.Where(predicate);
        }

        /// <summary>
        /// Project each element of a sequence into a new form base on predicates
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="condition"></param>
        /// <param name="predicateIfTrue"></param>
        /// <param name="predicateIfFalse"></param>
        /// <returns></returns>
        public static IEnumerable<object> SelectIf<T>(this IEnumerable<T> source, bool condition, Func<T, object> predicateIfTrue, Func<T, object> predicateIfFalse)
        {
            return condition
                ? source.Select(predicateIfTrue)
                : source.Select(predicateIfFalse);
        }
    }
}
