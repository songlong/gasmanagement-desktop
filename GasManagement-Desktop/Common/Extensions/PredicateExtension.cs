﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace GasManagement.Core.Common.Extensions
{
    public static class PredicateExtension
    {
        public static IQueryable<T> SearchByField<T>(this IQueryable<T> query, 
            string text, 
            Expression<Func<T, string>> field)
        {
            var tokens = text.SplitToken();
            return !tokens.Any()
                ? query
                : query.Provider.CreateQuery<T>(
                    tokens.Aggregate<string, MethodCallExpression>(null,
                        (currentPredicate, token) => query.CreateMethodCall(currentPredicate, field, x => x.Contains(token)))
                    );
        }

        public static MethodCallExpression CreateMethodCall<T>(this IQueryable query, Expression currentExpression, 
            Expression<Func<T, string>> field, Expression<Func<string, bool>> selectorPredicate)
        {
            var tAsParam = Expression.Parameter(typeof(T));
            var selectorExpressionBody = (MemberExpression)field.Body;
            var selectorPredicateBody = (MethodCallExpression)selectorPredicate.Body;
            MemberExpression memberExpression = Expression.MakeMemberAccess(tAsParam, selectorExpressionBody.Member);
            var invokeExp = Expression.Call(memberExpression, selectorPredicateBody.Method, selectorPredicateBody.Arguments);
            return Expression.Call(typeof(Queryable),
                "WHERE",
                new[] { query.ElementType },
                currentExpression ?? query.Expression,
                Expression.Lambda<Func<T, bool>>(invokeExp, tAsParam));
        }

        public static string[] SplitToken(this string text)
        {
            if (text.IsNullOrEmpty())
            {
                return new string[0];
            }
            return text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .Distinct()
                .ToArray();
        }
    }
}
