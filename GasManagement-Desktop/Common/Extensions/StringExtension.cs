﻿namespace GasManagement.Core.Common.Extensions
{
    public static class StringExtension
    {
        /// <summary>
        /// Indicates whether a specified string is null or empty
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string text)
        {
            return string.IsNullOrEmpty(text);
        }

        /// <summary>
        /// Indicates whether a specified string is null or white space
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpace(this string text)
        {
            return string.IsNullOrWhiteSpace(text);
        }

        /// <summary>
        /// Indicates whether a specified string contains another specified string
        /// </summary>
        /// <param name="container"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ContainsAsLower(this string container, string value)
        {
            return value == null || container == null ? false
                : container
                .ToLower()
                .Contains(value.ToLower());
        }
    }
}
