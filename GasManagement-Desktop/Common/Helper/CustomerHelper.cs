﻿using GasManagement.Core.Common.Dto.CustomerDto;
using GasManagement.Core.Models;
using System;
using System.Linq;

namespace GasManagement.Core.Common.Helper
{
    public static class CustomerHelper
    {
        public static RetailCustomerDetailDto ToRetailCustomerDetailDto(this RetailCustomer customer)
        {
            return customer == null
                ? null
                : new RetailCustomerDetailDto
                {
                    CustomerId = customer.Id,
                    Name = customer.Name,
                    Address = customer.Address,
                    Telephone = customer.ListRetailCustomerPhone!=null? String.Join(",", customer.ListRetailCustomerPhone.Select(x => x.PhoneNumber).ToList()):null,
                    Note = customer.Note,
                    ListGasCustomerName = customer.ListGasRetailCustomer != null ? string.Join(",", customer.ListGasRetailCustomer.Select(x => x.Gas?.Name)) : null,
                    ListGasCustomerId = customer.ListGasRetailCustomer != null ? string.Join(",", customer.ListGasRetailCustomer.Select(x => x.Gas?.Id)) : null,
                    Function = null
                };
        }

        public static WholesaleCustomerDetailDto ToWholesaleCustomerDetailDto(this WholesaleCustomer customer)
        {
            return customer == null
                ? null
                : new WholesaleCustomerDetailDto
                {
                    WholesaleCustomerId = customer.Id,
                    Name = customer.Name,
                    Address = customer.Address,
                    Telephone = customer.Telephone,
                    Note = customer.Note,
                    Function = null
                };
        }

        public static OrderRetailCustomerDto ToOrderRetailCustomerDto(this RetailOrder retailOrder)
        {
            var productName = retailOrder.GasId != null ? retailOrder.Gas?.AliasName : (retailOrder.ProductId != null ? retailOrder.Product?.AliasName : retailOrder.ProductCombo?.AliasName);
            return retailOrder == null
                ? null
                : new OrderRetailCustomerDto
                {
                    OrderDate = retailOrder.Date.ToString("dd/MM/yyyy"),
                    ProductName = productName,
                    EmployeeName = retailOrder.Employee?.AliasName,
                    OrderNote = retailOrder.Note ?? ""
                };
        }
    }
}
