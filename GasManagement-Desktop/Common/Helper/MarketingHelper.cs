﻿using GasManagement.Core.Common.Dto.MarketingDto;
using GasManagement.Core.Common.Dto.ProductDto;
using GasManagement.Core.Models;
using System.Linq;

namespace GasManagement.Core.Common.Helper
{
    public static class MarketingHelper
    {
        public static NewOrMissRetailCustomerDto ToNewOrMissRetailCustomerDto(this NewOrMissRetailCustomer newOrMissRetailCustomer)
        {
            return newOrMissRetailCustomer == null
                ? null
                : new NewOrMissRetailCustomerDto
                {
                    RetailCustomerId = newOrMissRetailCustomer.RetailCustomerId,
                    Address = newOrMissRetailCustomer.RetailCustomer.Address,
                    CustomerStatus = newOrMissRetailCustomer.CustomerStatus.Status,
                    DateCreated = newOrMissRetailCustomer.DateCreated,
                    DateTakeGas = newOrMissRetailCustomer?.DateTakeGas,
                    MarketingStaff = newOrMissRetailCustomer.Employee?.AliasName
                };
        }
    }
}
