﻿using GasManagement.Core.Common.Dto.OrderDto;
using GasManagement.Core.Models;
using System;
using System.Globalization;
using System.Linq;

namespace GasManagement.Core.Common.Helper
{
    public static class OrderHelper
    {
        public static RetailOrderDetailDto ToDto(this RetailOrder order)
        {
            return order == null
                ? null
                : new RetailOrderDetailDto
                {
                    OrderId = order.Id,
                    RetailCustomerId = order.Customer.Id,
                    Telephone = order.PhoneNumber ?? (order.Customer.ListRetailCustomerPhone != null ? String.Join(",", order.Customer.ListRetailCustomerPhone.Select(x => x.PhoneNumber).ToList()) : ""),
                    Address = order.Customer?.Address,
                    GasId = order.GasId,
                    GasAliasName = order.GasId.HasValue ? order.Gas.AliasName : string.Empty,
                    ProductId = order.ProductId,
                    ProductAliasName = order.ProductId.HasValue ? order.Product.AliasName : string.Empty,
                    ProductComboId = order.ProductComboId,
                    ProductComboAliasName = order.ProductComboId.HasValue ? order.ProductCombo.AliasName : string.Empty,
                    GiftId = order.GiftId,
                    GiftName = order.GiftId.HasValue ? order.Gift.Name : string.Empty,
                    EmployeeAliasName = order.EmployeeId.HasValue ? order.Employee.AliasName : string.Empty,
                    EmployeeId = order.EmployeeId.HasValue ? (int?)order.Employee.Id : null,
                    Note = order.Note ?? string.Empty,
                    Debt = order.RetailDebtOrder?.Select(x => x.TypeDebt.Description).ToList(),
                    RealMoneyCollect = order.RealMoneyTaken ?? 0,
                    ValuePoint = order.ValuePoint ?? 0
                };
        }

        public static RetailOrderSaleReport ToOrderPrintSaleReportDto(this RetailOrder order)
        {
            var productName = order.GasId != null ? order.Gas?.AliasName : (order.ProductId != null ? order.Product?.AliasName : order.ProductCombo?.AliasName);
            var productPrice = order.GasId != null ? order.Gas?.GasPrices.Where(x => x.Date <= order.Date && x.GasId == order.GasId).OrderByDescending(s => s.Date).Select(x => x.RetailPrice).FirstOrDefault()
                : (order.ProductId != null ? order.Product?.ProductPrice.Where(x => x.ProductId == order.ProductId).OrderByDescending(s => s.Date).Select(x => x.RetailPrice).FirstOrDefault()
                : order.ProductCombo?.ProductComboPrice.Where(x => x.ProductComboId == order.ProductComboId).OrderByDescending(s => s.Date).Select(x => x.RetailPrice).FirstOrDefault());
            return order == null
                ? null
                : new RetailOrderSaleReport
                {
                    Address = order.Customer?.Address,
                    ProductPrice = string.Format(new CultureInfo("vi-VN"), "{0:#,##0}", productPrice) + "đ",
                    ProductAliasName = productName,
                    Note = order.Note ?? string.Empty,
                    ValuePoint = order.ValuePoint.Value
                };
        }
    }
}
