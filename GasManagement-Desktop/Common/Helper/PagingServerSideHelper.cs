﻿using GasManagement.Core.Common.Dto.CommonDto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GasManagement.Core.Common.Helper
{
    public static class PagingServerSideHelper
    {
        public static async Task<PagingServerDto<TOut>> PageByServer<TIn, TOut>(this IQueryable<TIn> source, int start, int pageSize, int draw, Func<TIn, TOut> selector)
        {
            var totalRecord = await source.CountAsync().ConfigureAwait(false);
            var queryResult = source.Skip(start).Take(pageSize).ToArray();
            return new PagingServerDto<TOut>
            {
                PageIndex = start,
                PageSize = pageSize,
                Data = queryResult?.Select(selector).ToArray() ?? new TOut[0],
                RecordsTotal = totalRecord,
                Draw = draw
            };
        }
    }
}
