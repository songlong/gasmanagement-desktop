﻿using GasManagement.Core.Common.Dto;
using GasManagement.Core.Common.Dto.CustomerDto;
using GasManagement.Core.Models;
using System;
using System.Linq;

namespace GasManagement.Core.Common.Helper
{
    public static class LandlineHelper
    {
        public static CustomerLandlineDto ToRetailCustomerLandlineDto(this RetailCustomer customer)
        {
            return customer == null
                ? null
                : new CustomerLandlineDto
                {
                    CustomerId = customer.Id,
                    Name = customer.Name,
                    Address = customer.Address,
                    Note = customer.Note,
                    ListGasCustomerId = customer.ListGasRetailCustomer != null ? string.Join(",", customer.ListGasRetailCustomer.Select(x => x.Gas?.Id)) : null,
                };
        }

        public static OrderRetailCustomerLandlineDto ToOrderRetailCustomerLandlineDto(this RetailOrder retailOrder)
        {
            var productName = retailOrder.GasId != null ? retailOrder.Gas?.AliasName : (retailOrder.ProductId != null ? retailOrder.Product?.AliasName : retailOrder.ProductCombo?.AliasName);
            return retailOrder == null
                ? null
                : new OrderRetailCustomerLandlineDto
                {
                    RetailCustomerId = retailOrder.RetailCustomerId,
                    OrderDate = retailOrder.Date.ToString("dd/MM/yyyy"),
                    ProductName = productName,
                    EmployeeName = retailOrder.Employee?.AliasName,
                    OrderNote = retailOrder.Note ?? ""
                };
        }
    }
}
