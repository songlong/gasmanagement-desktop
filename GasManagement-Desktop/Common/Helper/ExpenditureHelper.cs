﻿using GasManagement.Core.Common.Dto;
using GasManagement.Core.Common.Dto.GasDto;
using GasManagement.Core.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace GasManagement.Core.Common.Helper
{
    public static class ExpenditureHelper
    {
        public static ExpenditureDetailDto ToExpenditureDetailDto(this Expenditure expenditure)
        {
            return expenditure == null
                ? null
                : new ExpenditureDetailDto
                {
                    ExpenditureId = expenditure.Id,
                    PaydayMoney = expenditure.PaydayMoney,
                    TypeExpenditure = expenditure.LExpenditure?.TypeExpenditure?.Name,
                    LExpenditure = expenditure.LExpenditure?.Name,
                    Money = expenditure.Money,
                    Note = expenditure.Note,
                    Function = null
                };
        }
    }
}
